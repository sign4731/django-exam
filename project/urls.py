from django.contrib import admin
from django.urls import path, include
from two_factor.views import LoginView, SetupView
from two_factor.urls import urlpatterns as tf_urls

from bank_app.views import index

urlpatterns = [
    path('admin/', admin.site.urls),
    path('bank/', include('bank_app.urls')),
    
    # 2FA
    path('', include(tf_urls)),
    path('', index, name='login'),
    path('setup/', SetupView.as_view(), name='setup'),

    # REST API
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]
