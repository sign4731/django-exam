import io
from django.shortcuts import render, get_object_or_404
from .models import Account, Customer, Ledger, Loan
from django.contrib.auth import logout as dj_logout
from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.conf import settings
from django.http import FileResponse
from reportlab.pdfgen import canvas
from datetime import date


def is_employee(user):
    return True if hasattr(user, 'employee') else False


def is_customer(user):
    return True if hasattr(user, 'customer') else False


def index(request):
    if request.user.is_authenticated:
        if not request.user.is_verified():
            # user not logged in using two-factor
            return HttpResponseRedirect(reverse('two_factor:setup'))

        # if user is employee, should not access /customer.html
        if is_customer(request.user):
            return HttpResponseRedirect(reverse('bank_app:customer'))
        elif is_employee(request.user):
            return HttpResponseRedirect(reverse('bank_app:employee'))
        else:
            logout(request)
            return HttpResponseRedirect(reverse('two_factor:login'))

    else:
        return HttpResponseRedirect(reverse('two_factor:login'))


@user_passes_test(is_customer, login_url='/')
def customer(request):
    context = {
        'customer': request.user.customer, 
        'bank_registration_number': settings.BANK_REGISTRATION_NUMBER
    }

    return render(request, 'bank_app/pages/customer/customer.html', context)


@user_passes_test(is_employee, login_url='/')
def employee(request):
    return render(request, 'bank_app/pages/employee/employee.html')


@user_passes_test(is_employee, login_url='/')
def customer_details(request, pk):
    customer = get_object_or_404(Customer, pk=pk)
    context = {
        'customer': customer
    }

    return render(request, 'bank_app/pages/employee/customer_details.html', context)


@user_passes_test(is_employee, login_url='/')
def get_customers_partial(request):
    customers = Customer.objects.all()
    context = {
        'customers': customers
    }

    return render(request, 'bank_app/partials/employee/customers_list.html', context)


@user_passes_test(is_employee, login_url='/')
def create_customer(request):
    if not request.method == 'POST':
        return HttpResponseRedirect(reverse('bank_app:index'))

    username = request.POST['username']
    first_name = request.POST['first_name']
    last_name = request.POST['last_name']
    phone = request.POST['phone']
    email = request.POST['email']
    password = request.POST['password']

    customer = Customer.create(username, first_name, last_name, phone, email, password)
    context = {
        'customers': [customer]
    }

    return render(request, 'bank_app/partials/employee/customers_list.html', context)

@login_required
def get_accounts_partial(request, pk):
    accounts = Account.objects.filter(user=pk)
    context = {
        'accounts': accounts
    }

    return render(request, 'bank_app/partials/accounts_list.html', context)


@user_passes_test(is_employee, login_url='/')
def create_account(request):
    if not request.method == 'POST':
        return HttpResponseRedirect(reverse('bank_app:index'))

    name = request.POST['name']
    pk = request.POST['pk']
    account = Account.create(name=name, pk=pk, is_loan=False)
    context = {
        'accounts': [account]
    }

    return render(request, 'bank_app/partials/accounts_list.html', context)
        

@user_passes_test(is_employee, login_url='/')
def set_rank(request):
    if not request.method == 'POST':
        return HttpResponseRedirect(reverse('bank_app:index'))

    pk = request.POST['pk']
    rank = request.POST['rank']
    customer = get_object_or_404(Customer, pk=pk)
    customer.set_rank(pk, rank)
    context = {
        'text': rank
    }

    return render(request, 'bank_app/partials/text.html', context)
    

@user_passes_test(is_customer, login_url='/')
def create_transfer(request):
    if not request.method == 'POST':
        return HttpResponseRedirect(reverse('bank_app:index'))

    from_account_id = request.POST['from_account_id']
    from_text = request.POST['from_text']

    to_account_id = request.POST['to_account_id']
    to_text = request.POST['to_text']
    to_registraton_number = request.POST['to_registraton_number']

    amount = float(request.POST['amount'])
    internal_bank_registration_number = settings.BANK_REGISTRATION_NUMBER
    banks = settings.BANKS

    context = {
        'customer': request.user.customer,
    }

    # Not internal transfer and external transfer
    if not to_registraton_number in banks and to_registraton_number != internal_bank_registration_number:
        context['message'] = {
            'text': 'Account does not exist',
            'type': 'error'
        }
        response = render(request, 'bank_app/partials/message.html', context)

    # Internal transfer
    elif to_registraton_number == internal_bank_registration_number:
        try:
            transaction_id = Ledger.internal_transfer(amount, from_account_id, to_account_id, from_text, to_text)
            context['message'] = {
                'text': 'Your money was transfered successfully',
                'type': 'success'
            }
            context['pdf_download'] = True
            context['transaction_id'] = transaction_id

            response = render(request, 'bank_app/partials/message.html', context)
            response.headers["HX-Trigger"] = "accountUpdate"
            
        except:
            context['message'] = {
                'text': 'Not enough money on account',
                'type': 'error'
            }

            response = render(request, 'bank_app/partials/message.html', context)
            response.headers["HX-Trigger"] = "accountUpdate"

    # Send external transfer
    else:
        transaction = Ledger.external_transfer(amount, from_account_id, to_account_id, from_text, to_text, to_registraton_number)
        context['message'] = transaction['message']
        
        if 'id' in transaction:
            context['pdf_download'] = True
            context['transaction_id'] = transaction['id']

        response = render(request, 'bank_app/partials/message.html', context)
        response.headers["HX-Trigger"] = "accountUpdate"

    return response


@user_passes_test(is_customer, login_url='/')
def request_loan(request):
    if not request.method == 'POST':
        return HttpResponseRedirect(reverse('bank_app:index'))

    name = request.POST['name']
    amount = float(request.POST['amount'])
    to_account_id = request.POST['to_account_id']

    context = {
        'customer': request.user.customer,
    }

    if not Account.objects.filter(pk=to_account_id).exists():
        context['message'] = {
            'text': 'Account does not exist.',
            'type': 'error'
        }
        return render(request, 'bank_app/partials/message.html', context)

    if not request.user.customer.can_make_loan:
        context['message'] = {
            'text': 'You are not high enough rank to make a loan. Please contact us.',
            'type': 'error'
        }
    else:
        context['message'] = {
            'text': 'You have successfully requested a loan. You will hear from us soon.',
            'type': 'success'
        }

    Loan.request(
        amount=amount,
        name=name, 
        user=request.user,
        to_account_id=to_account_id
    )

    response = render(request, 'bank_app/partials/message.html', context)
    response.headers["HX-Trigger"] = "loanUpdate"
    
    return response


@login_required
def get_loans_partial(request, pk=None):
    if is_employee(request.user):
        if pk:
            loans = Loan.objects.filter(user=pk)
        else:
            loans = Loan.objects.all()
        template = 'employee'
    else:
        loans = Loan.objects.filter(user=pk)
        template = 'customer'

    context = {
        'loans': loans 
    }
    
    return render(request, f'bank_app/partials/{template}/loans_list.html', context)


@user_passes_test(is_employee, login_url='/')
def set_loan_status(request, pk, new_status):
    if not request.method == 'POST':
        return HttpResponseRedirect(reverse('bank_app:index'))

    loan = get_object_or_404(Loan, pk=pk)

    if loan.status == "ACCEPTED" or loan.status == "DECLINED":
        context = { 
            'loan': loan, 
            'message': { 
                'text': f"Loan is already {loan.status}", 'type': 'error' 
            }
        }

        return render(request, 'bank_app/partials/message.html', context)

    if new_status == "ACCEPTED":
        loan.create(name=loan.name, customer_pk=loan.user.pk, amount=loan.amount, to_account_id=loan.to_account_id)

    loan.set_status(pk, new_status)

    context = {
        'loan': loan, 
        'message': { 
            'text': f"Loan status changed to {new_status}", 'type': 'success' 
        }
    }

    response = render(request, 'bank_app/partials/message.html', context)
    response.headers["HX-Trigger"] = "loanUpdate"

    return response


@user_passes_test(is_employee, login_url='/')
def get_loan_status(request, pk):
    loan = get_object_or_404(Loan, pk=pk)

    context = {
        'message': {
            'text': loan.status,
            'type': Loan.STATUS_COLORS[loan.status]
        }
    }

    return render(request, 'bank_app/partials/employee/loan_status.html', context)


@user_passes_test(is_employee, login_url='/')
def get_loan_actions(request, pk):
    loan = get_object_or_404(Loan, pk=pk)
    showActions = True if loan.status == 'PENDING' else False

    context = {
        'loan': loan,
        'showActions': showActions
    }

    return render(request, 'bank_app/partials/employee/loan_actions.html', context)


@user_passes_test(is_employee, login_url='/')
def loan_details(request, pk):
    loan = get_object_or_404(Loan, pk=pk)
    showActions = True if loan.status == 'PENDING' else False

    context =  {
        'loan': loan, 
        'showActions': showActions,
        'message': { 
            'text': loan.status, 
            'type': Loan.STATUS_COLORS[loan.status]
        }
    }

    return render(request, 'bank_app/pages/employee/loan_details.html', context)


@login_required
def account_details(request, pk):
    account = get_object_or_404(Account, pk=pk)
    is_owner_of_account = request.user.pk == account.user.pk

    if not is_owner_of_account and not is_employee(request.user):
        return HttpResponseRedirect(reverse('bank_app:index'))

    context = {
        'account': account
    }

    return render(request, 'bank_app/pages/account_details.html', context)


@login_required
def get_account_movements_partial(request, pk):
    account = get_object_or_404(Account, pk=pk)
    movements = account.movements.order_by('-timestamp')
    
    context = {
        'movements': movements
    }

    return render(request, 'bank_app/partials/account_movements.html', context)


@login_required
def logout(request):
    dj_logout(request)
    return HttpResponseRedirect(reverse('login'))


@user_passes_test(is_customer, login_url='/')
def download_receipt(request, transaction_id):
    transactions = Ledger.objects.filter(transaction_id=transaction_id).values()
    
    if not transactions:
        return HttpResponseRedirect(reverse('bank_app:index'))

    current_date = date.today().strftime('%d-%m-%Y')

    # Create a file-like buffer to receive PDF data.
    buffer = io.BytesIO()

    # Create the PDF object, using the buffer as its "file."
    pdf = canvas.Canvas(buffer)
    
    pdf.drawString(450, 750, current_date)
    # Title in browser tab
    pdf.setTitle('Transaction information')
    pdf.setFont('Helvetica', 20)
    pdf.drawString(100, 700, 'Transaction information' )

    x1 = 100
    y1 = 650
    x2 = 500

    for transaction in transactions:
        transfer_type = 'from' if transaction['amount'] < 0 else 'to'

        pdf.setFont('Helvetica-Bold', 12)
        pdf.drawString(x1, y1, 'Transfered money ' + transfer_type + ' account:' )
        y1 = y1 - 25

        pdf.setFont('Helvetica', 12)
        pdf.drawString(x1, y1, 'Account number:')
        y1 = y1 - 15

        pdf.drawString(x1, y1, str(transaction['account']))
        y1 = y1 - 25

        pdf.drawString(x1, y1, 'Reg. number:')
        y1 = y1 - 15

        pdf.drawString(x1, y1, transaction['registration_number'])
        y1 = y1 - 25

        pdf.drawString(x1, y1, 'Text:')
        y1 = y1 - 15

        pdf.drawString(x1, y1, transaction['text'])
        y1 = y1 - 25

        pdf.drawString(x1, y1, 'Amount:')
        y1 = y1 - 15

        pdf.drawString(x1, y1, str(transaction['amount']))
        y1 = y1 - 15

        pdf.line(x1, y1, x2, y1)
        y1 = y1 - 40

    # Close the PDF object cleanly, and we're done.
    pdf.showPage()
    pdf.save()

    # FileResponse sets the Content-Disposition header so that browsers
    # present the option to save the file.
    buffer.seek(0)
    return FileResponse(buffer, as_attachment=True, filename='transaction_receipt.pdf')
