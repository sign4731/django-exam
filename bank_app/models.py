from django.db import models, transaction
from django.contrib.auth.models import User
from django.conf import settings
import uuid
from decimal import Decimal
import requests
import json


class Customer(models.Model):
    user = models.OneToOneField(User, primary_key=True, on_delete=models.PROTECT)
    RANKS = (
        ("basic", "Basic"),
        ("silver", "Silver"),
        ("gold", "Gold"),
    )
    rank = models.CharField(max_length=300, choices=RANKS, default="Basic")
    phone = models.CharField(max_length=30)

    def __str__(self):
        return f"{self.pk} - {self.user} - {self.rank} - {self.phone}"

    @classmethod
    def create(cls, username, first_name, last_name, phone, email, password):
        user = User.objects.create_user(
            username=username,
            first_name=first_name,
            last_name=last_name,
            email=email,
            password=password
        )
        customer = Customer()
        customer.phone = phone
        customer.user = user
        customer.save()
        return customer

    @classmethod
    def set_rank(self, pk, rank):
        if (any(rank in i for i in self.RANKS)):
            customer = self.objects.get(pk=pk)
            customer.rank = rank
            customer.save()

    @property
    def can_make_loan(self):
        if self.rank == "Basic":
            return False
        else:
            return True

    @property
    def accounts(self):
        return Account.objects.filter(user=self.user)


class Employee(models.Model):
    user = models.OneToOneField(User, primary_key=True, on_delete=models.PROTECT)

    def __str__(self):
        return f"{self.pk} - {self.user}"

    @classmethod
    def create(cls, username, first_name, last_name, phone, email, password):
        user = User.objects.create_user(
            username=username,
            first_name=first_name,
            last_name=last_name,
            email=email,
            password=password
        )
        employee = Employee()
        employee.phone = phone
        employee.user = user
        employee.save()
        return employee


class Account(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, unique=True)
    user = models.ForeignKey(User, default="", on_delete=models.CASCADE)
    name = models.CharField(max_length=30)
    is_loan = models.BooleanField()

    def __str__(self):
        return f'{self.pk} - {self.user} - {self.name}'

    @property
    def balance(self):
        balance = Ledger.objects.filter(account=self.pk)
        balance = balance.aggregate(models.Sum('amount'))['amount__sum'] or Decimal(0)
        return balance

    @property
    def movements(self):
        return Ledger.objects.filter(account=self.pk)

    @classmethod
    def create(cls, name, pk, is_loan):
        account = Account()
        account.id = uuid.uuid4()
        account.user = User.objects.get(pk=pk)
        account.name = name
        account.is_loan = is_loan
        account.save()
        return account


class Loan(models.Model):
    STATUSES = (
        ("DECLINED", "Declined"),
        ("PENDING", "Pending"),
        ("ACCEPTED", "Accepted"),
    )

    STATUS_COLORS = {
        'PENDING': '',
        'DECLINED': 'error',
        'ACCEPTED': 'success',
    }

    name = models.CharField(max_length=30)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.CharField(max_length=16, choices=STATUSES, default="PENDING")
    amount = models.DecimalField(max_digits=100, decimal_places=2)
    to_account_id = models.UUIDField(default=uuid.uuid4)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.pk} - {self.name} - {self.user} - {self.amount} - {self.status} - {self.to_account}'

    @classmethod
    def request(self, name, user, amount, to_account_id):
        loan = Loan()
        loan.name = name
        loan.user = user
        loan.amount = amount
        loan.to_account_id = to_account_id
        loan.save()

        return loan

    @classmethod
    def create(self, name, customer_pk, amount, to_account_id):
        with transaction.atomic():
            # Create a loan account
            account = Account.create(name=name, pk=customer_pk, is_loan=True)

            # Make transfer = - on loan account, + on chosen account
            from_text = f"Loan: {name}"
            to_text = f"Loan: {name}"
            from_account_id = account.pk
            Ledger.internal_transfer(amount, from_account_id, to_account_id, from_text, to_text, is_loan=True)
        return account

    @classmethod
    def set_status(self, pk, status):
            loan = self.objects.get(pk=pk)
            loan.status = status
            loan.save()

class Ledger(models.Model):
    account = models.UUIDField()
    amount = models.DecimalField(max_digits=100, decimal_places=2)
    timestamp = models.DateTimeField(auto_now_add=True)
    text = models.CharField(max_length=300)
    transaction_id = models.UUIDField()
    registration_number = models.CharField(max_length=4)

    def __str__(self):
        return f'{self.account} - {self.amount} - {self.text}'

    @classmethod
    def internal_transfer(cls, amount, from_account_id, to_account_id, from_text, to_text, is_loan=False):
        transaction_id = uuid.uuid4()
        with transaction.atomic():
            from_account = Account.objects.get(pk=from_account_id)
            
            if from_account.balance >= amount or is_loan:
                Ledger.objects.create(amount=-amount, account=from_account_id, text=from_text, transaction_id=transaction_id, registration_number=settings.BANK_REGISTRATION_NUMBER)
                Ledger.objects.create(amount=amount, account=to_account_id, text=to_text, transaction_id=transaction_id, registration_number=settings.BANK_REGISTRATION_NUMBER)
            else:
                raise Exception("Not enough money on account")
        return transaction_id

    @classmethod
    def external_transfer(cls, amount, from_account_id, to_account_id, from_text, to_text, to_registraton_number):
        transaction_id = uuid.uuid4()
        api = ""
        banks = settings.BANKS
        bank_reg_number = settings.BANK_REGISTRATION_NUMBER
        context = {}

        try:
            if to_registraton_number in banks:
                api = banks[to_registraton_number]['api']

            transfer = {
                "amount": amount,
                "from_account_id": from_account_id,
                "to_account_id": to_account_id,
                "from_text": from_text,
                "to_text": to_text,
                "password": banks[bank_reg_number]['password']
            }
            response = requests.post(api, data=json.dumps(transfer))

            if response.status_code == 401:
                context['message'] = {
                    'text': 'Something went wrong, please try again later.',
                    'type': 'error'
                }
                return context
        except Exception as ex:
            print(ex)
            context['message'] = {
                'text': 'Something went wrong, please try again later.',
                'type': 'error'
            }
            return context

        with transaction.atomic():
            from_account = Account.objects.get(pk=from_account_id)
            if from_account.balance >= amount:
                Ledger.objects.create(amount=-amount, account=from_account_id, text=from_text, transaction_id=transaction_id, registration_number=bank_reg_number)
                Ledger.objects.create(amount=amount, account=to_account_id, text=to_text, transaction_id=transaction_id, registration_number=to_registraton_number)
            else:
                context['message'] = {
                    'text': 'Not enough money on account',
                    'type': 'error'
                }
                return context

        context['id'] = transaction_id
        context['message'] = {
            'text': 'Your money was transfered successfully',
            'type': 'success'
        }

        return context
