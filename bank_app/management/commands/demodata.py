from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from bank_app.models import Account, Employee, Customer, Loan

class Command(BaseCommand):
    def handle(self, **options):
        print('Adding data to the bank...')

        # Add a superuser
        if not User.objects.filter(username='admin').exists():
            User.objects.create_superuser('admin', '', 'admin1234')

        # Add the bank
        if not User.objects.filter(username='bank').exists():
            bank = Customer.create('bank', 'Bank', 'Bank', '12345678', 'bank@bank.com', 'bank1234')
            bank.set_rank(bank.pk, 'Gold')
            account = Account.create('bank account', bank.pk, False)

            # Add money to the bank
            Loan.create('Money to bank', bank.pk, 10000000, account.pk)

        # Add customer
        if not User.objects.filter(username='customer').exists():
            customer = Customer.create('customer', 'Customer', 'Customer', '12345678', 'customer@customer.com', 'customer1234')
            customer.set_rank(customer.pk, 'Gold')
            account = Account.create('customer account', customer.pk, False)
            account2 = Account.create('customer account 2', customer.pk, False)
            # Add money to the customer
            Loan.create('Money to customer', customer.pk, 1000, account.pk)
            Loan.request('Money for a house', customer.user, '250000', account.pk)
            Loan.request('Money for a car', customer.user, '50000', account2.pk)

        # Add an employee
        if not User.objects.filter(username='employee').exists():
            Employee.create('employee', 'Employee', 'Employee', '12345678', 'employee@employee.com', 'employee1234')

        print("Data successfully added to the bank...")
