from django.urls import path
from . import views
from . import apis

app_name = 'bank_app'

urlpatterns = [
    # Pages 
    path('index', views.index, name='index'),
    path('customer', views.customer, name='customer'),
    path('employee', views.employee, name='employee'),

    # Loan 
    path('loan_details/<int:pk>', views.loan_details, name='loan_details'),
    path('request_loan', views.request_loan, name='request_loan'),
    path('get_loans_partial/<int:pk>', views.get_loans_partial, name='get_loans_partial'),
    path('get_loans_partial/', views.get_loans_partial, name='get_loans_partial'),
    path('set_loan_status/<int:pk>/<str:new_status>/', views.set_loan_status, name='set_loan_status'),
    path('get_loan_status/<int:pk>/', views.get_loan_status, name='get_loan_status'),
    path('get_loan_actions/<int:pk>/', views.get_loan_actions, name='get_loan_actions'),

    # Account
    path('create_account', views.create_account, name='create_account'),
    path('account_details/<uuid:pk>', views.account_details, name='account_details'),
    path('get_accounts_partial/<int:pk>', views.get_accounts_partial, name='get_accounts_partial'),
    path('get_account_movements_partial/<uuid:pk>', views.get_account_movements_partial, name='get_account_movements_partial'),
    path('create_transfer', views.create_transfer, name='create_transfer'),
    path('api/external_transfer', apis.external_transfer, name="external_transfer"),

    # Customer 
    path('get_customers_partial', views.get_customers_partial, name='get_customers_partial'),
    path('create_customer', views.create_customer, name='create_customer'),
    path('customer_details/<int:pk>', views.customer_details, name='customer_details'),
    path('set_rank', views.set_rank, name='set_rank'),

    # Users
    path('logout', views.logout, name='logout'),

    # Transaction PDF
    path('download_receipt/<uuid:transaction_id>', views.download_receipt, name='download_receipt'),
]
