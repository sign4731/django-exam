import uuid

from .models import Account, Ledger
from django.conf import settings
from django.db import transaction

from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.parsers import JSONParser

@api_view(http_method_names=['POST'])
def external_transfer(request):
    banks = settings.BANKS
    transfer = JSONParser().parse(request)

    amount = transfer['amount']
    from_account_id = transfer['from_account_id']
    to_account_id = transfer['to_account_id']
    from_text = transfer['from_text']
    to_text = transfer['to_text']
    password = transfer['password']

    # Validate password - Does the bank exist?
    for index, bank in enumerate(banks):
        if banks[bank]['password'] == password:
            break
        
        print("Next")
        if index == len(banks) - 1:
            print("Wrong password")
            return Response({"message": "Bank not authorized."}, status="401")

    # Check if to_account_id exists within receiving bank else return error response to sending bank
    if not Account.objects.filter(pk=to_account_id).exists():
        return Response({"message": "Account doesn't exist."}, status=404)

    transaction_id = uuid.uuid4()
    with transaction.atomic():
        Ledger.objects.create(amount=-amount, account=from_account_id, text=from_text, transaction_id=transaction_id, registration_number=settings.BANK_REGISTRATION_NUMBER)
        Ledger.objects.create(amount=amount, account=to_account_id, text=to_text, transaction_id=transaction_id, registration_number=settings.BANK_REGISTRATION_NUMBER)

    return Response({"message": "Success"}, status=200)


