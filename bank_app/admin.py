from django.contrib import admin
from .models import Customer, Account, Employee, Ledger, Loan

admin.site.register(Customer)
admin.site.register(Account)
admin.site.register(Employee)
admin.site.register(Ledger)
admin.site.register(Loan)
