from django.test import TestCase, Client
from .models import Account, Customer, Employee

class CustomerTestCase(TestCase):
    def setUp(self):
        customer = Customer.create("larsmand", "Lars", "Larsen", "12345678", "lars@larsen.com", "lars1234")
        Account.create("Main account", customer.pk, False)

    def test_default_rank(self):
        customer = Customer.objects.get(user__username="larsmand")
        assert customer.rank == "Basic"

    def test_set_rank(self):
        customer = Customer.objects.get(user__username="larsmand")
        customer.set_rank("Gold")
        customer.refresh_from_db()

        assert customer.rank == "Gold"

    def test_can_make_loan_success(self):
        customer = Customer.objects.get(user__username="larsmand")
        customer.set_rank("Gold")
        customer.refresh_from_db()

        self.assertTrue(customer.can_make_loan)

    def test_can_make_loan_failure(self):
        customer = Customer.objects.get(user__username="larsmand")

        self.assertFalse(customer.can_make_loan)

    def test_customer_details_view(self):
        client = Client()
        client.login(username="larsmand", password="lars1234")
        customer = Customer.objects.get(user__username="larsmand")
        response = client.get(f"/bank/customer_details/{customer.pk}")

        assert response.status_code == 200
        assert response['content-type'] == 'text/html; charset=utf-8'
        self.assertTemplateUsed(response, 'bank_app/customer_details.html')
        self.assertIn('customer', response.context)

    def test_get_customers_partial(self):
        client = Client()
        client.login(username="larsmand", password="lars1234")
        response = client.get("/bank/get_customers_partial")

        assert response.status_code == 200
        assert response['content-type'] == 'text/html; charset=utf-8'
        self.assertTemplateUsed(response, 'bank_app/partials/employee/customers_list.html')
        self.assertIn('customers', response.context)


class TestViews(TestCase):
    def setUp(self):
        customer = Customer.create("larsmand", "Lars", "Larsen", "12345678", "lars@larsen.com", "lars1234")
        Account.create("Main account", customer.pk, False)
        Employee.create("admin", "Jens", "Jensen", "87654321", "admin@admin.com", "admin1234")

    def test_load_index_page_as_guest(self):
        client = Client()
        response = client.get("/")

        self.assertTemplateUsed(response, 'two_factor/core/login.html')
        assert response.status_code == 200
        assert response['content-type'] == 'text/html; charset=utf-8'

    def test_get_accounts_partial(self):
        client = Client()
        client.login(username="larsmand", password="lars1234")
        customer = Customer.objects.get(user__username="larsmand")
        response = client.get(f"/bank/get_accounts_partial/{customer.pk}")

        assert response.status_code == 200
        assert response['content-type'] == 'text/html; charset=utf-8'
        self.assertTemplateUsed(response, 'bank_app/partials/accounts_list.html')
        self.assertIn('accounts', response.context)

    def test_get_accounts_movements_partial(self):
        client = Client()
        client.login(username="larsmand", password="lars1234")
        customer = Customer.objects.get(user__username="larsmand")
        account = Account.objects.filter(user=customer.pk).first()
        response = client.get(f"/bank/get_account_movements_partial/{account.id}")

        assert response.status_code == 200
        assert response['content-type'] == 'text/html; charset=utf-8'
        self.assertTemplateUsed(response, 'bank_app/partials/account_movements.html')
        self.assertIn('movements', response.context)

    def test_create_customer_view(self):
        client = Client()
        client.login(username="admin", password="admin1234")
        response = client.post("/bank/create_customer", data={
            "username": "jensmand",
            "first_name": "Jens",
            "last_name": "Jensen",
            "phone": "87654321",
            "email": "jens@jensen.com",
            "password": "jens1234"
        })
        customer = Customer.objects.filter(user__username="jensmand").exists()

        self.assertTrue(customer)
        assert response.status_code == 200
        assert response['content-type'] == 'text/html; charset=utf-8'
        self.assertTemplateUsed(response, 'bank_app/partials/employee/customers_list.html')
        self.assertIn('customers', response.context)

    def test_create_account_view(self):
        client = Client()
        client.login(username="admin", password="admin1234")
        customer = Customer.objects.get(user__username="larsmand")
        response = client.post("/bank/create_account", data={
            "name": "Main account",
            "pk": customer.pk,
        })

        new_account_id = response.context['accounts'][0].id
        new_account = Account.objects.filter(id=new_account_id).exists()

        self.assertTrue(new_account)
        assert response.status_code == 200
        assert response['content-type'] == 'text/html; charset=utf-8'
        self.assertTemplateUsed(response, 'bank_app/partials/accounts_list.html')
        self.assertIn('accounts', response.context)

    def test_set_rank_view(self):
        client = Client()
        client.login(username="admin", password="admin1234")
        customer = Customer.objects.get(user__username="larsmand")
        response = client.post("/bank/set_rank", data={
            "pk": customer.pk,
            "rank": "Gold"
        })

        assert response.status_code == 200
        assert response['content-type'] == 'text/html; charset=utf-8'
        self.assertTemplateUsed(response, 'bank_app/partials/text.html')
        self.assertIn('rank', response.context)
        assert response.context['rank'] == "Gold"

    def test_account_details_view(self):
        client = Client()
        client.login(username="larsmand", password="lars1234")
        customer = Customer.objects.get(user__username="larsmand")
        account = Account.objects.filter(user=customer.pk).first()
        response = client.get(f"/bank/account_details/{account.id}")

        assert response.status_code == 200
        assert response['content-type'] == 'text/html; charset=utf-8'
        self.assertTemplateUsed(response, 'bank_app/account_details.html')
        self.assertIn('account', response.context)
