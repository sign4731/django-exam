from django.conf import settings

def bank_name(request):
  bank_reg_number = settings.BANK_REGISTRATION_NUMBER
  banks = settings.BANKS

  if bank_reg_number in banks:
    return {'bank_name': banks[bank_reg_number]['name']}
  else:
    return {}