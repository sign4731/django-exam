## How to install and run the project
> NOTE: You will need an authenticator app to log in to the bank!

1. Clone the project from this repository and navigate to the folder.
2. Make sure the Docker Desptop app is running
3. Run the command RTE=dev docker compose up to start the live server.
4. Navigate to http://127.0.0.1:8000/ in your brower
5. Log in as an employee with username employee and password employee1234 to create a customer account.
6. Now you’re all set!